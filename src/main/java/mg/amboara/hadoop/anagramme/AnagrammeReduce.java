/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.amboara.hadoop.anagramme;

import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author Amboara
 */
public class AnagrammeReduce extends Reducer<Text, Text, Text, Text> {
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
    {        
        Iterator<Text> i=values.iterator();
        String anagramme = "";
        while(i.hasNext()) {
            anagramme+= " " + i.next();   
        }
        context.write(key, new Text(anagramme));
    }
}
