/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.amboara.hadoop.anagramme;

import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
/**
 *
 * @author Amboara
 */
public class AnagrammeMap extends Mapper<Object, Text, Text, Text> {
    
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException
    {  
        StringTokenizer tok=new StringTokenizer(value.toString(), " ");
        while(tok.hasMoreTokens())
        {
            String wordString = tok.nextToken();
            Text word=new Text(wordString);
            Text anagramme=new Text(sortLetter(wordString));
            context.write(anagramme, word);
        }
    }
    
    public String sortLetter(String word) {
        String ret = "";
        char[] letters = word.toCharArray();
        char temp = letters[0];
        for(int i=1; i<letters.length; i++) {
            if(letters[i]>temp) {
                ret+=temp;
                temp=letters[i];
            } else {
                ret+=letters[i];
            }
        }
        return ret+temp;
    }
}
